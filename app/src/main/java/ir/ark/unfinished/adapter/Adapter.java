package ir.ark.unfinished.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

import ir.ark.unfinished.R;
import ir.ark.unfinished.object.ObjectRows;


/**
 * Created by Reyhane on 10/2/2017.
 */

public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    Context mContext;
    LayoutInflater inflater;
    public ArrayList<ObjectRows> data = null;
    ArrayList<Integer> visibleParents = new ArrayList<>();



    public Adapter(Context mContext, ArrayList<ObjectRows> data) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.data = data;
        visibleParents.add(0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getParentId();
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {



        boolean permission = false;

        for (int i = 0 ; i < visibleParents.size() ; i ++){


            if(visibleParents.get(i) == getItemViewType(position))
                permission = true;

        }



            try {
                if(permission){
                    final ViewHolderType0 holder = (ViewHolderType0) viewHolder;
                    holder.mytext.setText(data.get(position).getName());

                    holder.mytext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            visibleParents.add(data.get(position).getId());

                            for (int i = 0 ; i < visibleParents.size() ; i ++)
                                Log.e("test2002/ "+ i, visibleParents.get(i)+"");

                            notifyDataSetChanged();
                        }
                    });

                    return;
                }
            }catch (Exception ex){}











//        setAnimation(viewHolder.itemView, position);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = null;
        boolean permission = false;

        for (int i = 0 ; i < visibleParents.size() ; i ++){


            if(visibleParents.get(i) == viewType)
                permission = true;

        }


        if(permission){
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_inseide_unfinished, viewGroup, false);
            return new ViewHolderType0(v);}
        else {
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spacer_row, viewGroup, false);
            return new ViewHolderTypeSpacer(v);
        }







    }

    private int lastPosition = -1;
    //
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
    @Override
    public void onViewDetachedFromWindow(final RecyclerView.ViewHolder holder) {
        holder.itemView.clearAnimation();
    }
    public class ViewHolderType0 extends RecyclerView.ViewHolder {
        TextView mytext;






        ViewHolderType0(View view) {
            super(view);
            mytext = (TextView)view.findViewById(R.id.mytext);
        }
    }

    public class ViewHolderTypeSpacer extends RecyclerView.ViewHolder {

        ViewHolderTypeSpacer(View view) {
            super(view);

        }
    }


}
