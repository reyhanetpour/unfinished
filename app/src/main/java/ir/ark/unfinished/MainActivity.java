package ir.ark.unfinished;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ir.ark.unfinished.adapter.Adapter;
import ir.ark.unfinished.object.ObjectRows;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView_unfinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<ObjectRows> allDatas = new ArrayList<>(demoAnalysor("{\n" +
                "    \"categories\": [\n" +
                "        {\n" +
                "            \"id\": \"1\",\n" +
                "            \"title\": \"مدرسه احسان\",\n" +
                "            \"parent\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"2\",\n" +
                "            \"title\": \"مدرسه علوی\",\n" +
                "            \"parent\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"3\",\n" +
                "            \"title\": \"کلاس اول\",\n" +
                "            \"parent\": \"1\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"4\",\n" +
                "            \"title\": \"کلاس دوم\",\n" +
                "            \"parent\": \"1\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"5\",\n" +
                "            \"title\": \"کلاس سوم\",\n" +
                "            \"parent\": \"1\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"6\",\n" +
                "            \"title\": \"کلاس چهارم\",\n" +
                "            \"parent\": \"2\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"7\",\n" +
                "            \"title\": \"کلاس پنجم\",\n" +
                "            \"parent\": \"2\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"8\",\n" +
                "            \"title\": \"کلاس ششم\",\n" +
                "            \"parent\": \"2\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"9\",\n" +
                "            \"title\": \"ریاضی\",\n" +
                "            \"parent\": \"3\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"10\",\n" +
                "            \"title\": \"فیزیک\",\n" +
                "            \"parent\": \"3\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"11\",\n" +
                "            \"title\": \"شیمی\",\n" +
                "            \"parent\": \"4\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"12\",\n" +
                "            \"title\": \"زیست\",\n" +
                "            \"parent\": \"4\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"13\",\n" +
                "            \"title\": \"ادبیات\",\n" +
                "            \"parent\": \"4\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"14\",\n" +
                "            \"title\": \"فصل 1\",\n" +
                "            \"parent\": \"12\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"15\",\n" +
                "            \"title\": \"فصل 2\",\n" +
                "            \"parent\": \"12\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"16\",\n" +
                "            \"title\": \"فصل 3\",\n" +
                "            \"parent\": \"12\"\n" +
                "        }\n" +
                "    ]\n" +
                "}"));





        recyclerView_unfinish = (RecyclerView)findViewById(R.id.recyclerunfinished);
        recyclerView_unfinish.setLayoutManager(new LinearLayoutManager(this));
        recyclerView_unfinish.setAdapter(new Adapter(this, allDatas));

    }


    private ArrayList<ObjectRows> demoAnalysor(String input){

        ArrayList<ObjectRows> result = new ArrayList<>();


        try {

            JSONObject jsonObject = new JSONObject(input);
            JSONArray categories = jsonObject.getJSONArray("categories");

            for (int i = 0 ; i < categories.length() ; i ++){

                JSONObject categoryJsonObject = categories.getJSONObject(i);
                ObjectRows objectRows = new ObjectRows();

                objectRows.setId(categoryJsonObject.optInt("id"));
                objectRows.setName(categoryJsonObject.optString("title"));
                objectRows.setParentId(categoryJsonObject.optInt("parent"));

                result.add(objectRows);

            }

        }catch (JSONException js){}

        return result;
    }
}
