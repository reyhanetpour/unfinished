package ir.ark.unfinished.object;

import java.util.ArrayList;

/**
 * Created by Reyhane on 10/29/2017.
 */

public class ObjectRows {
    private int id, parentId;
    private String name ;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
